import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


# the purpose of this function is to take city and state information that
# comes in on the newly created location and assigns a photo pertaining
# to that location from our API, Pexels
# QUESTION: judging by the description of our get_photo function
#           ^^ what view function will we need to update?


    # Create a dictionary for the headers to use in the request
        # per the API documenation, all requests will need to include
        # your key, this is provided by adding an authorizatio header
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_photo(city, state):

    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    query = f'{city} {state}'
    url = f'https://api.pexels.com/v1/search?query={query}'
    # Make the request (make sure requests is downloaded .venv > lib > python > site packages)
    # requests is a package that needs to be downloaded, then imported at the top of this file
    response = requests.get(url, headers=headers)
    # Parse the JSON response
    picture_url = response.json()["photos"][0]["src"]["original"]

    return {
        "picture_url": picture_url
    }


def get_lat_lon(location):
    """
    Return teh latitude and longitude for the specified location using the
    OpenWeatherApp API
    """
    #   !! We WON'T use this in the views, it's just a helper function !!
    #   First, we need the lat and lon coordinates for the location because
    #   those are required parameters of the Weather Data API call

    #   1) construct the parameters to include in the url
    params = {
        "q": f'{location.city}, {location.state.abbreviation}, USA',
        "appid": OPEN_WEATHER_API_KEY,
    }
    #   2) define the url itself with the parameters
    url = 'http://api.openweathermap.org/geo/1.0/direct'
    #   3) make the request - the requests.get will pass long the parameters to the url and basically
    #      builds up the full url we need with the equal signs and ampersands
    response = requests.get(url, params)
    #   4) parse the response & pull out the lat and lon coordinates
    lat = response.json()[0]['lat']
    lon = response.json()[0]['lon']

    return {
        "lat": lat,
        "lon": lon,
    }


def get_weather_data(location):
    """
    Returns the current weather data for the specified location using
    the OpenWeatherMap API
    """

    #   get the lat and lon coordinates for the location
    lat_lon = get_lat_lon(location)

    #   if lat_long is None, return None
    if lat_lon is None:
        return None

    params = {
        "lat": lat_lon["lat"],
        "lon": lat_lon["lon"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }

    url = 'https://api.openweathermap.org/data/2.5/weather'

    response = requests.get(url, params)

    description = response.json()["weather"][0]["description"]
    temp = response.json()["main"]["temp"]

    return {
        "description": description,
        "temp": temp,
    }
